<?php
include_once '../include/header.php';
include_once '../../vendor/autoload.php';

$student = new \App\admin\Student\Student();
$data = $student->view($_GET['id']);

//var_dump($data);

?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Product</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Basic Product Add Form
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-8 col-md-offset-2">
                                <form role="form" action="view/student/update.php" method="POST" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label>Product Title</label>
                                        <input value="<?php echo $data['product_title']?>" name="product_title" class="form-control">
                                        <input type="hidden" name="id" value="<?php echo $data['id']?>" class="form-control">
                                        <input type="hidden" name="product_image" value="<?php echo $data['product_image']?>" class="form-control">

                                    </div>
                                    <div class="form-group">
                                        <label>Category</label>
                                        <select name="product_catagory" class="form-control">
                                            <option >Select One</option>
                                            <option <?php echo($data['product_catagory'] == 'Baby Products')? 'selected':''?> value="Baby Products">Baby Products</option>
                                            <option <?php echo($data['product_catagory'] == 'Stationery')? 'selected':''?> value="Stationery">Stationery</option>
                                            <option <?php echo($data['product_catagory'] == 'Vegetable')? 'selected':''?> value="Vegetable">Vegetable</option>
                                            <option <?php echo($data['product_catagory'] == 'Snacks')? 'selected':''?> value="Snacks">Snacks</option>
                                            <option <?php echo($data['product_catagory'] == 'Fish')? 'selected':''?> value="Fish">Fish</option>


                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Product Price</label>
                                        <input value="<?php echo $data['product_price']?>" name="product_price" class="form-control">
                                        <input type="hidden" name="id" value="<?php echo $data['id']?>" class="form-control">

                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea name="product_des" class="form-control" rows="3"><?php echo $data['product_des']?></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label >Update Image</label>
                                        <input type="file" name="product_image"/>
                                        <img width="250" src="assets/uploads/<?php echo $data['product_image']?>" alt="">
                                    </div>
                                    <div class="form-group">
                                        <button type="reset" class="btn btn-danger">Reset</button>
                                        <button type="submit" class="btn btn-primary">Update</button>
                                    </div>

                                    </div>

                                </form>
                            </div>
                        </div>
                        <!-- /.row (nested) -->
                    </div>
                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>