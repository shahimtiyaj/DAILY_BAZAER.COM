<?php
//session_start();
include_once '../include/header.php';
include_once '../../vendor/autoload.php';

?>

    <div id="page-wrapper" style="min-height: 349px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">All Product</h1>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <!-- /.row -->
        <div class="row">

            <?php

            $all_student = new \App\admin\Student\Student();
            $students = $all_student->index();
            $sl = 1;
            foreach ($students as $student){
                ?>

            <div class="col-md-3 col-sm-6">
    		<span class="thumbnail">
                <img src="assets/uploads/<?php echo $student['product_image']?>" width="250"  alt="">
      			<h4><?php echo $student['product_title']?></h4>
      			<div class="ratings">
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star-empty"></span>
                </div>
      			<p> <?php echo $student['product_des']?> </p>
      			<hr class="line">
      			<div class="row">
      				<div class="col-md-6 col-sm-6">
      					<p class="price">$<?php echo $student['product_price']?></p>
      				</div>
      				<div class="col-md-6 col-sm-6">
      				 <a href="view/student/view.php?id=<?php echo $student['id'] ?>" target="_blank" >	<button class="btn btn-info right" > Details</button></a>
      				</div>

      			</div>
    		</span>
            </div>
            <?php } ?>
            <!-- END PRODUCTS -->
            </div>
        <!-- /.row -->
    </div>

<?php
include_once '../include/footer.php';
?>