<?php
namespace App;
use PDO;
use PDOException;
class Helper extends Connection {

   static public function upload_img(){

        $file_name = $_FILES['product_image']['name'];
        $file_tmp = $_FILES['product_image']['tmp_name'];
        $fileName = substr(md5(time()),'0','8');

        $extName = end(explode('.',$file_name));

        $_POST['product_image'] = $fileName.'.'.$extName;

        move_uploaded_file($file_tmp,'../../assets/uploads/'.$_POST['product_image']);

       return $_POST['product_image'];
    }

    public function delete_img($id){
        $stmt = $this->con->prepare("SELECT `product_image` FROM `product` WHERE `product`.`id` = :id");
        $stmt->execute(array(':id' => $id));
        $image_name = $stmt->fetch(PDO::FETCH_ASSOC);
            $dat = '../../assets/uploads/'.$image_name['product_image']; // uploads is a folder name or path
            if(isset($dat)){
                unlink($dat);
            }

    }

}