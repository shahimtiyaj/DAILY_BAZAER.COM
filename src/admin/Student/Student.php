<?php
namespace App\admin\Student;
session_start();
use App\Connection;
use PDOException;
use PDO;
class Student extends Connection
{
    private $product_title;
    private $product_catagory;
    private $product_des;
    private $product_price;
    private $product_image;
    private $id;
    public function set($data = array()){
       if (array_key_exists('product_title', $data)) {
           $this->product_title = $data['product_title'];
       }
        if (array_key_exists('product_catagory', $data)) {
            $this->product_catagory = $data['product_catagory'];
        }
        if (array_key_exists('product_des', $data)) {
            $this->product_des = $data['product_des'];
        }
        if (array_key_exists('product_price', $data)) {
            $this->product_price = $data['product_price'];
        }
        if (array_key_exists('product_image', $data)) {
            $this->product_image = $data['product_image'];
        }

        if (array_key_exists('id', $data)) {
            $this->id = $data['id'];
        }


        return $this;
   }



   public function store(){

     try{
         $stmt = $this->con->prepare("INSERT INTO `product` (`product_title`, `product_catagory`,`product_des`,`product_price`,`product_image`) VALUES(:product_title,:product_catagory,:product_des,:product_price,:product_image)");
         $result =  $stmt->execute(array(
             ':product_title' => $this->product_title,
             ':product_catagory' => $this->product_catagory,
             ':product_des' => $this->product_des,
             ':product_price' => $this->product_price,
             ':product_image' => $this->product_image
         ));
         if($result){
             $_SESSION['msg'] = 'Data successfully Inserted !!';
             header('location:index.php');
         }
     }catch (PDOException $e) {
         echo "There is some problem in connection: " . $e->getMessage();
     }

   }

    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `product`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }


    public function view($id){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `product` WHERE id=:id");
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            return $stmt->fetch(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function delete($id){
        try{
            $stmt = $this->con->prepare("DELETE FROM `product` WHERE id=:id");
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                $_SESSION['delete'] = 'Data successfully Deleted !!';
                header('location:index.php');
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }


    public function update(){
        try{
            $stmt = $this->con->prepare("UPDATE `php_oop_crud`.`product` SET `product_title` = :product_title, `product_catagory` = :product_catagory, `product_des` = :product_des,`product_price` = :product_price,`product_image` = :product_image WHERE `product`.`id` = :id;");
            $stmt->bindValue(':product_title', $this->product_title, PDO::PARAM_INT);
            $stmt->bindValue(':product_catagory', $this->product_catagory, PDO::PARAM_INT);
            $stmt->bindValue(':product_des', $this->product_des, PDO::PARAM_INT);
            $stmt->bindValue(':product_price', $this->product_price, PDO::PARAM_INT);
            $stmt->bindValue(':product_image', $this->product_image, PDO::PARAM_INT);

            $stmt->bindValue(':id', $this->id, PDO::PARAM_INT);
            $stmt->execute();
            if($stmt){
                $_SESSION['update'] = 'Data successfully Updated !!';
                header('location:index.php');
            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

}